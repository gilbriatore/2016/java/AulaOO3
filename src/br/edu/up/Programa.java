package br.edu.up;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Programa {
	
	public static void main(String[] args){
	
		//Cria um objeto pessoa;
		Pessoa p1 = new Pessoa();
		p1.setNome("Jo�o"); 
		p1.setIdade(25);
		p1.setSexo("Masculino"); 
		
		//Cria um objeto aluno;
		Aluno a1 = new Aluno();
		a1.setNome("Maria");
		a1.setIdade(20);
		a1.setSexo("Feminino");
		a1.setDisciplina("Matem�tica"); 
		a1.setNota(10);
		
		//Cria um objeto professor;
		Professor pr1 = new Professor();
		pr1.setNome("Pedro");
		pr1.setIdade(60);
		pr1.setSexo("Masculino");
		pr1.setDisciplina("Matem�tica");
		
		//Cria a lista de pessoas;
		List<Pessoa> lista = new ArrayList<Pessoa>();
		
		//Cria um objeto de leitura;
		Scanner leitor = new Scanner(System.in);
		
		//Adiciona os objetos pessoa, aluno e professor
		//na lista;
		lista.add(p1);
		lista.add(a1);
		lista.add(pr1);
		
		//Preenche a lista de pessoas;
		for (int i = 0; i < 2; i++) {
			Pessoa p = new Pessoa();
			System.out.println("Digite o nome: ");
			p.setNome(leitor.nextLine());
			System.out.println("Digite a idade: ");
			p.setIdade(Integer.parseInt(leitor.nextLine()));
			System.out.println("Digite o sexo: ");
			p.setSexo(leitor.nextLine());
			
			lista.add(p);	
		}
		leitor.close();
		
		//Imprimir as pessoas da lista;
		for (Pessoa p : lista) {
			System.out.println("Nome: " + p.getNome());
			System.out.println("Idade: " + p.getIdade() );
			System.out.println("Sexo: " + p.getSexo()); 
			System.out.println("---------------------");
		}
		
		
		
		
		
		
		
		
		
	}
}